class AddStatusToKomodos < ActiveRecord::Migration
  def change
    add_column :komodos, :name, :string
    add_column :komodos, :age, :integer
  end
end
