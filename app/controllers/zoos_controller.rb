class ZoosController < ApplicationController
  def index
    @zoos = Zoo.all
  end

  def new
    @zoo = Zoo.new
  end

  def create
    @zoo = Zoo.new(zoo_params)
    @zoo.save

    redirect_to :action => :index
  end

  def show
    @zoo = Zoo.find(params[:id])
  end

  def edit
    @zoo = Zoo.find(params[:id])
  end

  def update
    @zoo = Zoo.find(params[:id])
    @zoo.update(zoo_params)

    redirect_to :action => :show, :id => @zoo
  end

  def destroy
    @zoo = Zoo.find(params[:id])
    @zoo.destroy

    redirect_to :action => :index
  end

  private
  def zoo_params
    params.require(:zoo).permit(:name, :age)
  end
end
