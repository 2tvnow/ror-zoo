class KomodosController < ApplicationController
  def index
    @komodos = Komodo.all
  end

  def new
    @komodo = Komodo.new
  end

  def create
    @komodo = Komodo.new(komodo_params)
    @komodo.save

    redirect_to :action => :index
  end

  def show
    @komodo = Komodo.find(params[:id])
  end

  def edit
    @komodo = Komodo.find(params[:id])
  end

  def update
    @komodo = Komodo.find(params[:id])
    @komodo.update(komodo_params)

    redirect_to :action => :show, :id => @komodo
  end

  def destroy
    @komodo = Komodo.find(params[:id])
    @komodo.destroy

    redirect_to :action => :index
  end

  private
  def komodo_params
    params.require(:komodo).permit(:name, :age)
  end

end
